/* code for the "ga" pd class. */

#include "m_pd.h"
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
 
typedef struct ga
{
  t_object x_ob;
  t_outlet *x_outlet;
    
    // ga parameters
    int Npopulation;
    int strLen;
    float crossoverProb;
    float mutationProb;
    float targetFitness;
    float inputBlend;
    
    // ga structures
    int *input;
    int **pop;
    int **newPop;
    int **parents;
    int **children;
    float *distance;
    float *selectionCDF;
    float *selectionProb;
    int shouldTerminate[3];
    
    
} t_ga;

t_class *ga_class;

/* function prototypes */
// pd functions
void ga_bang(t_ga *x);
void ga_set(t_ga *x, t_symbol *selector, int argcount, t_atom *argvec);
void ga_set_Npopulation(t_ga *x, int newNpopulation);
void ga_list(t_ga *x, t_symbol *selector, int argc, t_atom *argv);
void *ga_new(void);
void ga_setup(void);
// genetic algorithm functions
void ga_initialize(int **pop, int *input, int Npopulation, int strLen, float inputBlend);
void ga_evaluate(float *distance, int *input, int **pop, int Npopulation, int strLen);
void ga_selection(int **parents, float *selectionProb, float *selectionCDF, int **pop, float *distance, int Npopulation, int strLen);
void ga_crossover(int **children, int **parents, int strLen);
void ga_mutate(int **newPop, float mutationProb, int Npopulation, int strLen);
void ga_evolve(int **newPop, int **pop, int *input, float *distance, int **parents, int **children, float *selectionProb, float *selectionCDF, float crossoverProb, float mutationProb, int Npopulation, int strLen);
void ga_terminate(int *shouldTerminate, float *distance, int *input, int **newPop, float targetFitness, int Npopulation, int strLen);


// left inlet bang callback
/* this is the same as the list callback except we don't initialize a population
 and we don't take in a new input string
 this doesn't seem as useful since we don't get very interesting results back
 they kind of repeat because we always choose the fittest individual from the population
*/
void ga_bang(t_ga *x)
{
    
    // run until we reach our target fitness or until we've evolved 1000 generations
    int i, r, c;
    for(i = 0; i < 1000; i++)
    {
        // run 1 evolution
        ga_evolve(x->newPop, x->pop, x->input, x->distance, x->parents, x->children, x->selectionProb, x->selectionCDF, x->crossoverProb, x->mutationProb, x->Npopulation, x->strLen);
        
        // check if we should terminate
        ga_terminate(x->shouldTerminate, x->distance, x->input, x->newPop, x->targetFitness, x->Npopulation, x->strLen);
        if(x->shouldTerminate[0] == 1)
        {
            break;
            
        }
        
        // copy new population into pop
        for(r = 0; r < x->Npopulation; r++)
        {
            for(c = 0; c < x->strLen; c++)
            {
                x->pop[r][c] = x->newPop[r][c];
            }
        }
        
    }
    
    // output the fittest one right now (newPop[shouldTerminate[1]])
    t_atom fittestInd[x->strLen];
    for(i = 0; i < x->strLen; i++)
    {
        SETFLOAT(fittestInd+i, x->newPop[x->shouldTerminate[1]][i]);
    }
    outlet_list(x->x_outlet, gensym("list"), x->strLen, fittestInd);
    
    
    post("have we reached the target fitness? should terminate: %d, fittest index: %d, maxFitness: %d", x->shouldTerminate[0], x->shouldTerminate[1], x->shouldTerminate[2]);
    
}

// left inlet "set" message callback
void ga_set(t_ga *x, t_symbol *selector, int argcount, t_atom *argvec)
{
    
    //post("set: selector %s", selector->s_name);
    
    int i;
    
    if(argcount != 2)
    {
        post("please format your set message as: set param value");
        return;
    }
    
    for (i = 0; i < argcount; i++)
    {
        if (argvec[i].a_type == A_FLOAT)
        {
            post("you can only set one of the following: Npopulation, targetFitness, crossOverProb, mutationProb, inputBlend");
        }
        else if (argvec[i].a_type == A_SYMBOL)
        {
            if(strcmp(argvec[i].a_w.w_symbol->s_name, "Npopulation") == 0)
            {
                //x->Npopulation = (int)argvec[i+1].a_w.w_float;
                int newNpopulation = (int)argvec[i+1].a_w.w_float;
                // re-initialize all internal structures
                ga_set_Npopulation(x, newNpopulation);
                post("Npopulation set to %d", x->Npopulation);
                break;
            } else if(strcmp(argvec[i].a_w.w_symbol->s_name, "crossoverProb") == 0)
            {
                x->crossoverProb = argvec[i+1].a_w.w_float;
                post("crossoverProb set to %f", argvec[i+1].a_w.w_float);
                break;
            } else if(strcmp(argvec[i].a_w.w_symbol->s_name, "mutationProb") == 0)
            {
                x->mutationProb = argvec[i+1].a_w.w_float;
                post("set mutationProb to %f", argvec[i+1].a_w.w_float);
                break;
            } else if(strcmp(argvec[i].a_w.w_symbol->s_name, "targetFitness") == 0)
            {
                x->targetFitness = argvec[i+1].a_w.w_float;
                post("set targetFitness to %f", argvec[i+1].a_w.w_float);
                break;
            }  else if(strcmp(argvec[i].a_w.w_symbol->s_name, "inputBlend") == 0)
            {
                x->inputBlend = argvec[i+1].a_w.w_float;
                post("set inputBlend to %f", argvec[i+1].a_w.w_float);
                break;
            }  else
            {
                post("you can only set one of the following: Npopulation, targetFitness, crossOverProb, mutationProb, inputBlend");
                break;
            }
            
        }
    }
    
}

// reset internal structures because Npopulation has been changed
void ga_set_Npopulation(t_ga *x, int newNpopulation)
{
    // I'm going to do this with free and malloc, but I might be
    // able to change this to just realloc() in the future
    
    // free structures first
    int i,j;
    for (i = 0; i < x->Npopulation; ++i) {
        free(x->pop[i]);
        free(x->newPop[i]);
    }
    free(x->pop);
    free(x->newPop);
    free(x->distance);
    free(x->selectionProb);
    free(x->selectionCDF);
    
    // set the internal Npopulation variable to the new size
    x->Npopulation = newNpopulation;
    
    // allocate memory for all those variables with the new population size
    x->pop = (int **)malloc(sizeof(int *) * x->Npopulation);
    x->newPop = (int **)malloc(sizeof(int *) * x->Npopulation);
    for(i = 0; i < x->Npopulation; i++)
    {
        x->pop[i] = (int *)malloc(sizeof(int) * x->strLen);
        x->newPop[i] = (int *)malloc(sizeof(int) * x->strLen);
    }
    for(i = 0; i < x->Npopulation; i++)
    {
        for(j = 0; j < x->strLen; j++)
        {
            x->pop[i][j] = 0;
            x->newPop[i][j] = 0;
        }
    }
    
    x->distance = (float *)malloc(sizeof(float) * x->Npopulation);
    x->selectionProb = (float *)malloc(sizeof(float) * x->Npopulation);
    x->selectionCDF = (float *)malloc(sizeof(float) * x->Npopulation);
    for(i = 0; i < x->Npopulation; i++)
    {
        x->distance[i] = 0.0f;
        x->selectionProb[i] = 0.0f;
        x->selectionCDF[i] = 0.0f;
    }
}

// left inlet list callback
void ga_list(t_ga *x, t_symbol *selector, int argc, t_atom *argv)
{
    
    // parse input list
    int i;
    for(i = 0; i < argc; i++)
    {
        if(i > x->strLen)
            return;
            
        // error check
        if(argv[i].a_type != A_FLOAT)
        {
            post("arguments in list must be numbers");
            return;
        }
        x->input[i] = atom_getfloatarg(i, argc, argv);
    }
    
    
    // initialize the population
    //ga_initialize(x->pop, x->Npopulation, x->strLen);
    ga_initialize(x->pop, x->input, x->Npopulation, x->strLen, x->inputBlend);
    
    // run until we reach our target fitness or until we've evolved 1000 generations
    int r, c;
    for(i = 0; i < 1000; i++)
    {
        // run 1 evolution
        ga_evolve(x->newPop, x->pop, x->input, x->distance, x->parents, x->children, x->selectionProb, x->selectionCDF, x->crossoverProb, x->mutationProb, x->Npopulation, x->strLen);
         
        // check if we should terminate
        ga_terminate(x->shouldTerminate, x->distance, x->input, x->newPop, x->targetFitness, x->Npopulation, x->strLen);
        if(x->shouldTerminate[0] == 1)
        {
            break;
           
        }
        
        // copy new population into pop
        for(r = 0; r < x->Npopulation; r++)
        {
            for(c = 0; c < x->strLen; c++)
            {
                x->pop[r][c] = x->newPop[r][c];
            }
        }
        
         
    }
    
    
    // output the fittest one right now (newPop[shouldTerminate[1]])
    t_atom fittestInd[x->strLen];
    for(i = 0; i < x->strLen; i++)
    {
        SETFLOAT(fittestInd+i, x->newPop[x->shouldTerminate[1]][i]);
    }
    outlet_list(x->x_outlet, gensym("list"), x->strLen, fittestInd);
    

    post("have we reached the target fitness? should terminate: %d, fittest index: %d, maxFitness: %d", x->shouldTerminate[0], x->shouldTerminate[1], x->shouldTerminate[2]);
    
}

// new/constructor function
void *ga_new(void)
{
    t_ga *x = (t_ga *) pd_new(ga_class);
    post("new ga object");
    
    // create a new outlet, save pointer returned
    x->x_outlet = outlet_new(&x->x_ob, gensym("list"));
    
    // initialize variables
    srand(time(NULL));  // seed the random number generator
    //srand(123);
    
    // default settings for now (move this and set them as object variables later)
    x->Npopulation = 50;
    x->strLen = 32;
    x->crossoverProb = 0.6f;
    x->mutationProb = 0.06f;
    x->targetFitness = 0.1f;
    x->inputBlend = 0.0f;
    
    // initialize structures
    int i, j;
    
    x->input = (int *)malloc(sizeof(int) * x->strLen);
    for(i = 0; i < x->strLen; i++)
    {
        x->input[i] = 0;
    }
    
    x->pop = (int **)malloc(sizeof(int *) * x->Npopulation);
    x->newPop = (int **)malloc(sizeof(int *) * x->Npopulation);
    for(i = 0; i < x->Npopulation; i++)
    {
        x->pop[i] = (int *)malloc(sizeof(int) * x->strLen);
        x->newPop[i] = (int *)malloc(sizeof(int) * x->strLen);
    }
    for(i = 0; i < x->Npopulation; i++)
    {
        for(j = 0; j < x->strLen; j++)
        {
            x->pop[i][j] = 0;
            x->newPop[i][j] = 0;
        }
    }
    
    x->parents = (int **)malloc(sizeof(int *) * 2);
    x->children = (int **)malloc(sizeof(int *) * 2);
    for(i = 0; i < 2; i++)
    {
        x->parents[i] = (int *)malloc(sizeof(int) * x->strLen);
        x->children[i] = (int *)malloc(sizeof(int) * x->strLen);
    }
    for(i = 0; i < 2; i++)
    {
        for(j = 0; j < x->strLen; j++)
        {
            x->parents[i][j] = 0;
            x->children[i][j] = 0;
        }
    }
    
    x->distance = (float *)malloc(sizeof(float) * x->Npopulation);
    x->selectionProb = (float *)malloc(sizeof(float) * x->Npopulation);
    x->selectionCDF = (float *)malloc(sizeof(float) * x->Npopulation);
    for(i = 0; i < x->Npopulation; i++)
    {
        x->distance[i] = 0.0f;
        x->selectionProb[i] = 0.0f;
        x->selectionCDF[i] = 0.0f;
    }
    
    x->shouldTerminate[0] = 0;
    x->shouldTerminate[1] = 0;
    x->shouldTerminate[2] = 100;
    
    
    return (void *)x;
    
}

// setup function
void ga_setup(void)
{
    // create a new class
    ga_class = class_new(gensym("ga"), (t_newmethod)ga_new,
    	0, sizeof(t_ga), 0, A_GIMME, 0);
    
    // register the bang callback
    class_addbang(ga_class, (t_method)ga_bang);
    // callback for the "set" message
    class_addmethod(ga_class, (t_method)ga_set, gensym("set"), A_GIMME, 0);
    // register the list callback
    class_addlist(ga_class, (t_method)ga_list);
    
    
}


/* genetic algorithm functions */


/* 
 ga_initialize(int **pop, int Npopulation, int strLen): generates an initial population
 of the correct population size and string length
 inputs:
    - pop: the population to fill (a matrix)
    - input: the input string
    - Npopulation: the size of the population to create
    - strLen: the length of the strings within the population
    - inputBlend: percentage of initial population that should be copies of the input
 */
void ga_initialize(int **pop, int *input, int Npopulation, int strLen, float inputBlend)
{
    
    int numInput = (int)floor(inputBlend * Npopulation);
    
    int i, j;
    float r;
    
    // generate random numbers for each inidividual in the population
    for(i = 0; i < Npopulation; i++)
    {
        if(i < numInput)
        {
            for(j = 0; j < strLen; j++)
            {
                pop[i][j] = input[j];
            }
        } else
        {
            for(j = 0; j < strLen; j++)
            {
                r = (float)rand()/RAND_MAX; // random number between 0 and 1
                // snap to 0 or 1 for random binary number
                if(r < 0.5f)
                    pop[i][j] = 0;
                else
                    pop[i][j] = 1;
            }
        }
    }
    
}

/*
 evaluation (using Hamming distance/xor)
 input:
    - distance: the distance array to fill in, it has length equal to Npopulation
    - input: the binary string input (horizontal vector)
    - pop: the population of size (#individuals, stringLength)
    - Npopulation: the number of individuals in our population
    - strLen: the length of individuals in our population
 */
void ga_evaluate(float *distance, int *input, int **pop, int Npopulation, int strLen)
{
    
    int i, j, s;
    for(i = 0; i < Npopulation; i++)
    {
        s = 0;
        for(j = 0; j < strLen; j++)
        {
            if(input[j] != pop[i][j])
                s = s + 1;
        }
        distance[i] = (float)s/strLen;
    }

}

/*
 selection - roulette wheel selection
 selects two parents with probability proportional to their fitness
 inputs:
    - parents: where to store the parents as an array of 2 parents where each parents is of length strLen
    - selectionProb: a vector of selection probabilities (this gets filled in by the function, just keep it all 0s and of length=Npopulation)
    - selectionCDF: a vector of the cumulative sum of the selection probailities (this gets filled in by the function, just keep it all 0s and of length=Npopulation)
    - pop: the population to select from of size (#individuals, stringLength)
    - distance: the Hamming distance measure of each individual from the
        input string (vertical vector of length #individuals)
    - Npopulation: the number of individuals in our population
    - strLen: the length of individuals in our population
 
*/
void ga_selection(int **parents, float *selectionProb, float *selectionCDF, int **pop, float *distance, int Npopulation, int strLen)
{
    // fill in selection probability vector
    int i, j;
    float distanceSum = 0;
    for(i = 0; i < Npopulation; i++)
    {
        distanceSum += distance[i];
    }
    float oneOverDistanceSum = 1.0f/distanceSum;
    for(i = 0; i < Npopulation; i++)
    {
        selectionProb[i] = (1.0f - distance[i])*oneOverDistanceSum;
    }
   
    // fill in selection cumulative distribution function to choose from
    for(i = 0; i < Npopulation; i++)
    {
        if(i > 0)
            selectionCDF[i] = selectionCDF[i-1] + selectionProb[i];
        else
            selectionCDF[i] = selectionProb[i];
            
    }

    // we need to select 2 parents to make the next generation
    float r1 = (float)rand()/RAND_MAX;
    float r2 = (float)rand()/RAND_MAX;
    
    // post("parents: %f, %f", r1, r2);
    
    // pick out parent indices from rand number and CDF
    for(i = 0; i < Npopulation; i++)
    {
        if(r1 < selectionCDF[i])
        {
            // post("parent 1: %d", i);
            for(j = 0; j < strLen; j++)
            {
                parents[0][j] = pop[i][j];
            }
            break;
        }
    }
    for(i = 0; i < Npopulation; i++)
    {
        if(r2 < selectionCDF[i])
        {
            // post("parent 2: %d", i);
            for(j = 0; j < strLen; j++)
            {
                parents[1][j] = pop[i][j];
            }
            break;
        }
    }
    
}

/* 
 crossover - chooses a random point in the string and performs crossover to form 2
 children
 inputs:
    - children: an array of 2 children where each child is of length strLen (this is filled in by the function)
    - parents: an array of 2 parents where each parents is of length strLen
    - strLen: the length of the parents (and also the individuals in our population)
 outputs:
 
*/

void ga_crossover(int **children, int **parents, int strLen)
{
    
    float r = (float)rand()/RAND_MAX;
    int crossoverPt = (int)floor(r*strLen);
    
    // post("crossover point: %d", crossoverPt);
    
    // we can probably use memcpy or something like that here, but let's do a loop for now
    // at least until we figure out that we might need to do something faster
    int i;
    for(i = 0; i < crossoverPt; i++)
    {
        children[0][i] = parents[0][i];
        children[1][i] = parents[1][i];
    }
    for(i = crossoverPt; i < strLen; i++)
    {
        children[0][i] = parents[1][i];
        children[1][i] = parents[0][i];
    }
    

}


/* 
 mutation - goes through the entire newly generated population and sees if each
 individual needs to be mutated or not. if one must be mutated, we flip a
 random bit
 inputs:
    - newPop: the newly generated population of size (#individuals, stringLength) - this is filled in by the function
    - mutationProb: the probability that we mutate an individual
    - Npopulation: the number of individuals in our population
    - strLen: the length of individuals in our population
 */
void ga_mutate(int **newPop, float mutationProb, int Npopulation, int strLen)
{
    // loop through entire population
    int i;
    float r;
    int mutationPt;
    for(i = 0; i < Npopulation; i++)
    {
        // check if should we mutate this individual
        r = (float)rand()/RAND_MAX;
        if(r < mutationProb)
        {
            // mutate (aka flip a random bit)
            
            //post("mutating index %d", i);
            
            r = (float)rand()/RAND_MAX;
            mutationPt = (int)floor(r*strLen);
            newPop[i][mutationPt] = !newPop[i][mutationPt];
        }
    }


}

/*
 evolve() - create new population (evolve one generation)
 this loops through selection and crossover until the new population is
 full. then it loops through the entire new population and handles
 mutation.
 inputs:
    - newPop: the newly generated population (#individuals, stringLength) - gets filled in by this function
    - pop: the population to create a new one from of size
        (#individuals, stringLength)    
    - input: the binary string input (horizontal vector)
    - distance: array of length Npopulation (can be empty, just a storage place for the function)
    - parents: 2 parents, each of length strLen (can be empty, just a storage place for the function)
    - children: 2 children, each of length strLen (can be empty, just a storage place for the function)
    - selectionProb: a vector of selection probabilities (this gets filled in by the function, just keep it all 0s and of length=Npopulation)
    - selectionCDF: a vector of the cumulative sum of the selection probailities (this gets filled in by the function, just keep it all 0s and of length=Npopulation)
    - crossoverPob: the probability for crossover
    - mutationProb: the probability for mutation
    - Npopulation: the number of individuals in our population
    - strLen: the length of individuals in our population
 output:
 
*/

void ga_evolve(int **newPop, int **pop, int *input, float *distance, int **parents, int **children, float *selectionProb, float *selectionCDF, float crossoverProb, float mutationProb, int Npopulation, int strLen)
{
    
    // evaluate the current population
    ga_evaluate(distance, input, pop, Npopulation, strLen);
    
    // selection and crossover loop
    // for each iteration, we choose 2 parents then, we either move
    // the 2 parents into the new population or we perform crossover
    // and move their 2 children into the new population
    // we only need to loop through half of the size of the population
    int s;
    float r;
    int i;
    
    for(i = 0; i < Npopulation; i+=2)
    {
        // select 2 parents
        ga_selection(parents, selectionProb, selectionCDF, pop, distance, Npopulation, strLen);
        
        // check if we should do crossover
        r = (float)rand()/RAND_MAX;
        if(r < crossoverProb)
        {
            // crossover and put children in new population
            ga_crossover(children, parents, strLen);
            for(s = 0; s < strLen; s++)
            {
                newPop[i][s] = children[0][s];
                newPop[i+1][s] = children[1][s];
            }
        } else
        {
            // no crossover; put parents in the new population
            for(s = 0; s < strLen; s++)
            {
                newPop[i][s] = parents[0][s];
                newPop[i+1][s] = parents[1][s];
            }
        }
    
    }
    
    // mutation
    ga_mutate(newPop, mutationProb, Npopulation, strLen);
    
    
}


/*
 termination - check for termination
 find the fittest individual f in the new population and compare its fitness
 to the targetFitness. if the f's fitness is > targetFitness, we can
 terminate. this function will return the fittest individual regardless of
 whether or not we should terminate
 inputs:
    - shouldTerminate: an array where the first index (0) is True/False for whether we should terminate or not,
        the second index (1) is the index of the fittest individual, the third index (2) is the fitness of that individual  
        this gets filled in by the function
    - distance: array of length Npopulation (can be empty, just a storage place for the function)
    - input: the input string/rhythmic pattern
    - newPop: the population to check for termination of size (#individuals, stringLength)
    - targetFitness: the fitness value that we want to achieve [0,1]
    - Npopulation: the number of individuals in our population
    - strLen: the length of individuals in our population
 outputs:
 
*/
void ga_terminate(int *shouldTerminate, float *distance, int *input, int **newPop, float targetFitness, int Npopulation, int strLen)
{
    // evaluate the current population
    ga_evaluate(distance, input, newPop, Npopulation, strLen);
    
    // get the most fit individual (find the min and argmin of the distance array)
    float maxFitness = 1.0f;
    int fittestInd = 0;
    int i;
    for(i = 0; i < Npopulation; i++)
    {
        if(distance[i] < maxFitness)
        {
            maxFitness = distance[i];
            fittestInd = i;
        }
    }

    // fill in our output structure
    shouldTerminate[1] = fittestInd;
    shouldTerminate[2] = (int)100*maxFitness;
    if(maxFitness < targetFitness)
        shouldTerminate[0] = 1;
    else
        shouldTerminate[0] = 0;
    

}

